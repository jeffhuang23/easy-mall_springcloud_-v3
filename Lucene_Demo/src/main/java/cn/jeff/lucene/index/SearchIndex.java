package cn.jeff.lucene.index;


import cn.jeff.lucene.analyzer.IKAnalyzer6x;
import javafx.scene.control.IndexRange;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SearchIndex {
    /**
     *   索引搜索的方式
     */
    @Test
    public void termQuery() throws IOException {
        /**
         * 词项搜索
         * 最基础的搜索方式
         */
        //指定索引文件目录
        Path path = Paths.get("d:/index01");
        FSDirectory dir = FSDirectory.open(path);
        //搜索对象执行搜索时, 要对索引进行计算, 读取数据, 基于输入流reader创建
        IndexReader reader = DirectoryReader.open(dir);
        IndexSearcher searcher = new IndexSearcher(reader);
        //创建查询条件
        //创建一个termQuery 词项查询条件 "title" : "美国"
        Term term = new Term("title", "美国");
        //创建查询结果对象
        Query query = new TermQuery(term);
        //浅查询逻辑, 获取数据信息docId读取数据索引表
        //在倒排索引表中读取TopDocs
        TopDocs top10 = searcher.search(query, 10);//读取TopDocs前10条索引
        System.out.println("查询总条数: "+ top10.totalHits);
        //获取对应前10条索引, 返回ScoreDoc数组
        ScoreDoc[] scoreDocs = top10.scoreDocs;
        //从索引数组中根据document的id 读取对应document的数据
        for (ScoreDoc sd : scoreDocs) {
            //元素对象中包含了documentId
            int docId = sd.doc;
            //获取到对应的Document对象
            Document doc = searcher.doc(docId);
            System.out.println("title: " + (doc.get("title")==null?"没有数据":doc.get("title")));
            System.out.println("content: " + (doc.get("content")== null?"没查到":doc.get("content")));
            System.out.println("press: " + (doc.get("press") == null ?"没查到":doc.get("press")));
            System.out.println("click: " + (doc.get("click") == null ?"没查到":doc.get("click")));
        }
    }

    @Test
    public void MultiFieldQuery() throws ParseException, IOException {
        /**
         * 多域查询
         * 对多个词项进行查询
         */
        Path path = Paths.get("d:/index01");
        FSDirectory dir = FSDirectory.open(path);
        DirectoryReader reader = DirectoryReader.open(dir);
        IndexSearcher searcher = new IndexSearcher(reader);

        String[] fields = {"title","content"};
        //创建多域查询条件
        MultiFieldQueryParser parser = new MultiFieldQueryParser(fields, new IKAnalyzer6x());
        Query query = parser.parse("中国美国");
        //底层实现逻辑
        //ik分词器对"中国美国"进行分词--->"中国","美国","国美"
        //和String[] 中的域名做 排列组合
        TopDocs top10 = searcher.search(query, 10);
        ScoreDoc[] scoreDocs = top10.scoreDocs;
        System.out.println("查询总条数: "+ top10.totalHits);
        //从索引数组中根据document的id 读取对应document的数据
        for (ScoreDoc sd : scoreDocs) {
            //元素对象中包含了documentId
            int docId = sd.doc;
            //获取到对应的Document对象
            Document doc = searcher.doc(docId);
            System.out.println("title: " + (doc.get("title")==null?"没有数据":doc.get("title")));
            System.out.println("content: " + (doc.get("content")== null?"没查到":doc.get("content")));
            System.out.println("press: " + (doc.get("press") == null ?"没查到":doc.get("press")));
            System.out.println("click: " + (doc.get("click") == null ?"没查到":doc.get("click")));
        }

    }

    @Test
    public void BooleanQuery() throws IOException {
        /**
         * 布尔查询
         */
        Path path = Paths.get("d:/index01");
        FSDirectory dir = FSDirectory.open(path);
        DirectoryReader reader = DirectoryReader.open(dir);
        IndexSearcher searcher = new IndexSearcher(reader);

        //创建查询条件
        //boolean条件
        //子条件
        TermQuery query1 = new TermQuery(new Term("title", "美国"));
        TermQuery query2 = new TermQuery(new Term("content", "中国"));
        //封装子条件
        /**
         * @MUST  布尔查询结果必须是子条件的子集
         * @MUST_NOT  查询结果不属于子条件的子集
         */
        BooleanClause bc1 = new BooleanClause(query1, BooleanClause.Occur.MUST_NOT);
        BooleanClause bc2 = new BooleanClause(query2, BooleanClause.Occur.MUST);
        BooleanQuery booleanQuery = new BooleanQuery.Builder().add(bc1).add(bc2).build();

        TopDocs top10 = searcher.search(booleanQuery, 10);
        ScoreDoc[] scoreDocs = top10.scoreDocs;
        System.out.println("查询总条数: "+ top10.totalHits);
        //从索引数组中根据document的id 读取对应document的数据
        for (ScoreDoc sd : scoreDocs) {
            //元素对象中包含了documentId
            int docId = sd.doc;
            //获取到对应的Document对象
            Document doc = searcher.doc(docId);
            System.out.println("title: " + (doc.get("title")==null?"没有数据":doc.get("title")));
            System.out.println("content: " + (doc.get("content")== null?"没查到":doc.get("content")));
            System.out.println("press: " + (doc.get("press") == null ?"没查到":doc.get("press")));
            System.out.println("click: " + (doc.get("click") == null ?"没查到":doc.get("click")));
        }

    }

    @Test
    public void RangeSearch() throws IOException {
        /**
         * 范围查询
         */

        Path path = Paths.get("d:/index01");
        FSDirectory directory = FSDirectory.open(path);
        IndexReader reader = DirectoryReader.open(directory);
        IndexSearcher searcher = new IndexSearcher(reader);

        //创建查询条件
        //搜索点击数在50到100的所有document集合
        Query query = IntPoint.newRangeQuery("click",50,100);
        //先从倒排索引表中计算, 获取前10条
        TopDocs topDocs = searcher.search(query, 10);
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        //从数组中解析前10条的documentId
        for (ScoreDoc sc:scoreDocs) {
            int docId = sc.doc;
            Document doc = searcher.doc(docId);
            System.out.println("title"+ doc.get("title"));
            System.out.println("content"+ doc.get("content"));
            System.out.println("press"+ doc.get("press"));
            System.out.println("click"+ doc.get("click"));
        }

    }

    @Test
    public void FuzzySearch() throws IOException {
        /**
         * 模糊查询
         */
        Path path = Paths.get("d:/index01");
        FSDirectory directory = FSDirectory.open(path);
        DirectoryReader reader = DirectoryReader.open(directory);
        IndexSearcher searcher = new IndexSearcher(reader);

        //创建模糊搜索条件
        Query query = new FuzzyQuery(new Term("title", "America"));
        TopDocs topDocs = searcher.search(query, 10);
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        for (ScoreDoc sc: scoreDocs) {
            int docId = sc.doc;
            Document doc = searcher.doc(docId);
            System.out.println("title: "+ doc.get("title"));
            System.out.println("content: "+ doc.get("content"));
            System.out.println("press: "+ doc.get("press"));
            System.out.println("click: "+ doc.get("click"));
        }
    }

    @Test
    public void wildCartQuery() throws IOException {
        /**
         * 通配查询
         */
        Path path = Paths.get("d:/index01");
        FSDirectory directory = FSDirectory.open(path);
        DirectoryReader reader = DirectoryReader.open(directory);
        IndexSearcher searcher = new IndexSearcher(reader);

        //创建通配搜索条件
        Query query = new WildcardQuery(new Term("content", "中?"));
        TopDocs topDocs = searcher.search(query, 10);
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        for (ScoreDoc sc: scoreDocs) {
            int docId = sc.doc;
            Document doc = searcher.doc(docId);
            System.out.println("title: "+ doc.get("title"));
            System.out.println("content: "+ doc.get("content"));
            System.out.println("press: "+ doc.get("press"));
            System.out.println("click: "+ doc.get("click"));
        }
    }
}
