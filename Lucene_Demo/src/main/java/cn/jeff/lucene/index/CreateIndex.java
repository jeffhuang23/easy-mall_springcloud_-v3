package cn.jeff.lucene.index;

import cn.jeff.lucene.analyzer.IKAnalyzer6x;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateIndex {
    //实现lucene 的 创建索引文件
    @Test
    public void createIndex() throws IOException {
        //指定到一个文件目录 d:/index01
        Path path = Paths.get("d:/index01");
        //路径交给lucene管理,作为索引文件目录
        FSDirectory dir = FSDirectory.open(path);
        //创造一个输出流对象writer
        //配置输出流的配置对象config
        //指定分词器
        IndexWriterConfig config = new IndexWriterConfig(new IKAnalyzer6x());
        //创建索引文件的模式
        //将索引文件的创建模式设定为每次将旧数据覆盖
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        IndexWriter writer = new IndexWriter(dir, config);
        //对Document对象添加属性
        Document doc1 = new Document();
        Document doc2 = new Document();
        //拼接数据到document, 将两个网页数据进行封装
        /**
         * document类中的field 域属性的使用
         * @name 域属性名称
         * @value 源数据
         * @store 是否存储到索引中
         */
        //手动将数据封装成document对象
        doc1.add(new TextField("title", "American 美国", Field.Store.YES));
        doc1.add(new TextField("content","甩锅中国 blame to China",Field.Store.YES));
        doc1.add(new TextField("press","新华社",Field.Store.YES));
        //数字类型数据 IntPoint LongPoint DoublePoint FloatPoint
        //数字类型不进行分词和存储
        doc1.add(new IntPoint("click", 58));
        doc1.add(new StringField("click","58次",Field.Store.YES));

        doc2.add(new TextField("title","England 英国",Field.Store.YES));
        doc2.add(new StringField("content","中国 Chinese",Field.Store.YES));
        doc2.add(new IntPoint("click",33));
        doc2.add(new StringField("click","33次点击",Field.Store.YES));
        //将document 通过writer输出
        writer.addDocument(doc1);
        writer.addDocument(doc2);
        //生成索引文件
        writer.commit();

    }
}
