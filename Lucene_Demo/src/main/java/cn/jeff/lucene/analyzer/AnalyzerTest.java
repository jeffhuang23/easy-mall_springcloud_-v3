package cn.jeff.lucene.analyzer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;

public class AnalyzerTest {
    /**
     * 完成方法, 接收文本字符串,
     * 通过使用分词器的api将词项的文本属性
     */

    public void printTerm(Analyzer analyzer, String msg) throws IOException {
        //使用analyzer 实现对象, 解析msg分词计算
        //string 元数据转化成流计算
        StringReader reader = new StringReader(msg);
        //调用analyzer 这个分词的api 将read 流计算成词项
        TokenStream token = analyzer.tokenStream("test", reader);// 分词不能独立存在,依托
        token.reset(); //将指针回到最开始的位置
        //拿到当前指针位置的词项的文本属性
        OffsetAttribute offsetAttribute = token.getAttribute(OffsetAttribute.class);
        CharTermAttribute charTermAttribute = token.getAttribute(CharTermAttribute.class);
        while (token.incrementToken()) {
            System.out.println("偏移量开始位置:" + offsetAttribute.startOffset());
            System.out.println("偏移量结束位置:" + offsetAttribute.endOffset());
            System.out.println(charTermAttribute.toString());
        }
    }

    @Test
    public void run() throws IOException {
        //构造多个不同实现类的分词器
//        Analyzer a1 = new StandardAnalyzer();
//        Analyzer a2 = new SimpleAnalyzer();
//        Analyzer a3 = new WhitespaceAnalyzer();
//        Analyzer a4 = new SmartChineseAnalyzer();
        Analyzer a5 = new IKAnalyzer6x();

        String msg = "王者荣耀  Chinese and American";
        //计算分词的文本字符串
//        System.out.println("---------------标准分词器---------");
//        printTerm(a1, msg);
//        System.out.println("---------------简单分词器---------");
//        printTerm(a2, msg);
//        System.out.println("---------------空格分词器---------");
//        printTerm(a3, msg);
//        System.out.println("---------------中文分词器---------");
//        printTerm(a4, msg);
        System.out.println("---------------IK分词器----------");
        printTerm(a5, msg);
    }
}
