package cn.jeff.es;

import com.carrotsearch.hppc.ObjectLookupContainer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.common.pojo.Product;
import org.apache.lucene.util.QueryBuilder;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.admin.indices.settings.get.GetSettingsRequestBuilder;
import org.elasticsearch.action.admin.indices.settings.get.GetSettingsResponse;
import org.elasticsearch.action.get.GetRequestBuilder;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.AdminClient;
import org.elasticsearch.client.ClusterAdminClient;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Set;

public class ESTest {
    //通过私有属性, 通过@Before 构建一个连接对象
    private TransportClient client;

    @Before
    //给私有属性赋值,提供ip port
    public void init() throws UnknownHostException {
        //给client 赋值
        //传递一个setting值, 定义连接的集群名称elasticsearch
        /*Settings setting =
                Settings.builder().put("cluster.name","elasticsearch").build();*/
        client = new PreBuiltTransportClient(Settings.EMPTY);
        //收集ip和端口的对象address, 连接协调器的端口
        InetSocketTransportAddress address =
                new InetSocketTransportAddress(InetAddress.getByName("10.42.0.56"),9300);
        //添加到client对象
        client.addTransportAddress(address);
    }


    @Test
    public void indexManage() throws IOException {
        /**
         *      集群的索引管理
         *      创建索引, 删除索引, 获取查看索引
         *      transportclient 封装了操作逻辑
         *      对索引对集群的操作, 需要先获取admin管理对象
         */
        //创建索引 http://10.42.12.113:9200/index01
        //获取admin 可以管理集群cluster , 可以管理indices
        AdminClient admin = client.admin();
        //操作索引的对象
        IndicesAdminClient indices = admin.indices();
        ClusterAdminClient cluster = admin.cluster();

        //建一个索引  index06
        //indices.create(); 方法调用, 命令就直接发送
        //prepare 方法调用方法不直接发送请求, 获取了返回值request对象
        //创建索引的请求对象
        CreateIndexRequestBuilder createIndexRequest =
                indices.prepareCreate("index06");
        //通过请求对象进行设置
        createIndexRequest.setSettings(Settings.builder().put("number_of_shards","3").put("number_of_replicas","1"));
        //通过请求对象创建自定义映射
//        createIndexRequest.addMapping("doc","{\"mappings\":{\"properties\":{\"content\":{\"type\":\"text\",\"analyzer\":\"ik_max_word\"}}}}");

        CreateIndexResponse createIndexResponse = createIndexRequest.get();
        //response解析就是创建索引的返回值{"acknowledge","shards"}
        createIndexResponse.isShardsAcked();
        createIndexResponse.isAcknowledged();

       /* //
        GetAliasesRequestBuilder getAliasesResquest = indices.prepareGetAliases("index06");
        System.out.println(getAliasesResquest.get().getAliases().get(""));
        GetMappingsRequestBuilder getMappingsRequest = indices.prepareGetMappings("index02");
        GetSettingsRequestBuilder getSettingsRequestBuilder = indices.prepareGetSettings("index05");
        IndicesExistsRequestBuilder existsRequestBuilder = indices.prepareExists("index06");

        GetMappingsResponse getMappingsResponse = getMappingsRequest.get();
        ImmutableOpenMap<String, ImmutableOpenMap<String, MappingMetaData>> mappings = getMappingsResponse.mappings();
        ObjectLookupContainer<String> keys = mappings.keys();
        MappingMetaData mappingMetaData = mappings.get("index02").get("article");
        String type = mappingMetaData.type();
        System.out.println("当前mapping类型有:"+type);
        Map<String, Object> stringObjectMap = mappingMetaData.sourceAsMap();
        Set<Map.Entry<String, Object>> entries = stringObjectMap.entrySet();
        for(Map.Entry<String, Object> entry:entries){
            System.out.println("key"+entry.getKey());
            System.out.println("value"+entry.getValue());
        }
*/
    }

    @Test
    public void docManage() throws JsonProcessingException {
        /**
         * 文档管理
         * 添加document
         */
        //读取数据源
        Product product = new Product();
        product.setProductId("123456");
        product.setProductCategory("手机");
        product.setProductImgurl("http://image.jt.com");
        product.setProductName("华为手机");
        product.setProductPrice(50.0);
        product.setProductNum(10);

        //将地址转化Json
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(product);
        //通过client的prepare方法获取request对象
        IndexRequestBuilder request =
                client.prepareIndex("index06", "product", product.getProductId());
        //request添加请求数据
        request.setSource(json);
        IndexResponse indexResponse = request.get();
        //新建document 返回字符串json
        long version = indexResponse.getVersion();
        System.out.println("创建version: "+ version);

        /**
         * 删除document
         */
        client.prepareDelete("index06","product","123456");
        /**
         * 获取
         */

        GetRequestBuilder getRequest =
                client.prepareGet("index06", "product", "123456");
    }

    @Test
    public void search(){
        //通过query对象的封装
        //搜索的过程的分页是浅查询
        TermQueryBuilder query = QueryBuilders.termQuery("title", "java");
        SearchRequestBuilder request = client.prepareSearch("index02");

        //搜索
        request.setQuery(query);
        //搜索分页查询,起始位置, 条数
        request.setFrom(0);
        request.setSize(5);

        SearchResponse searchResponse = request.get();
        //从response中解析结果
        SearchHits topDocs = searchResponse.getHits();
        System.out.println("查询总数:" + topDocs.totalHits);

        //遍历数组
        SearchHit[] hits = topDocs.getHits();
        for (SearchHit hit:hits) {
            //获取一个document返回结果
            System.out.println("source的json: "+hit.getSourceAsString()
            );
        }
    }
}
