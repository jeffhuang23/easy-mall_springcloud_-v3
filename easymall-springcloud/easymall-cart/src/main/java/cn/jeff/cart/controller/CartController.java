package cn.jeff.cart.controller;

import cn.jeff.cart.service.CartService;
import com.jt.common.pojo.Cart;
import com.jt.common.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cart/manage")
public class CartController {
    @Autowired
    private CartService cartService;
    @RequestMapping("query")
    public List<Cart> queryMyCarts(String userId){
        /**
         * 根据userId 查询用户的购物车
         */
        return cartService.queryMyCarts(userId);
    }

    @RequestMapping("save")
    public SysResult addCart(Cart cart){//cart中只有userId 和 productId num=1
        try{
            cartService.addCart(cart);
            return SysResult.ok();
        }catch (Exception e){
            e.printStackTrace();
            return SysResult.build(201,"新增购物车失败", null);
        }
    }

    @RequestMapping("update")
    public SysResult updateNum(Cart cart){
        /**
         * 更新商品功能
         * cart 中包含userId,productid,num
         */
        try{
            cartService.updateNum(cart);
            return SysResult.ok();
        }catch (Exception e){
            e.printStackTrace();
            return SysResult.build(201, "更新num失败",null);
        }
    }

    @RequestMapping("delete")
    public SysResult deleteCart(Cart cart){//cart 中只有userId, productId
        try{
            cartService.deleteCart(cart);
            return SysResult.ok();
        }catch (Exception e){
            e.printStackTrace();
            return SysResult.build(201, "删除购物车失败",null);
        }
    }
}
