package cn.jeff.cart.service;

import cn.jeff.cart.mapper.CartMapper;
import cn.jeff.product.ProductApi;
import com.jt.common.pojo.Cart;
import com.jt.common.pojo.Product;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    @Autowired(required = false)
    private CartMapper cartMapper;

    @Override
    public List<Cart> queryMyCarts(String userId) {
        return cartMapper.selectCartByUserId(userId);
    }

   /*  ribbon组件注入
    @Autowired
    private RestTemplate restTemplate;*/

    /*注入feign的组件*/
    @Autowired
    private ProductApi productApi;

    @Override
    @HystrixCommand(fallbackMethod = "error")
    public void addCart(Cart cart) {
        /**
         * 新增购物车功能
         * 先判断cart中是否已经有该商品, 如果有就update修改商品数量, 没有就是insert
         */
        //通过数据库查询商品数据是否已经存在
        Cart existCart = cartMapper.selectExistCartByUserIdAndProductId(cart);
        if (existCart != null){
            //说明购物车商品在数据库已经存在
            //把已存在的商品num+ 新增商品 num
            // 这个sql方式不能和更新商品数量功能共用,需要重新写sql
            //update t_cart set num = num+#{num}
           cart.setNum(cart.getNum()+existCart.getNum());
            cartMapper.updateCartNumByUserIdAndProductId(cart);
        }else {
            //需要新增一个cart对象到数据库
            //cart对象中只有userId, productId num 没有商品的信息
            //需要调用商品系统的单个商品查询功能

         /* 使用ribbon组件的restTemplate 进行服务调用
            String url = "http://ProductService/product/manage/item/"+cart.getProductId();
            Product pro = restTemplate.getForObject(url, Product.class);*/

            Product pro = productApi.queryProduct(cart.getProductId());
            if (pro != null){
                //获得了商品对象后,将商品信息加入cart对象
                cart.setProductImage(pro.getProductImgurl());
                cart.setProductName(pro.getProductName());
                cart.setProductPrice(pro.getProductPrice());
                //商品数量已经传入了num=1
                cartMapper.insertCart(cart);
            }else {
                throw new RuntimeException("商品系统找不到商品信息,新增购物车失败");
            }

        }
    }

    //新增购物车的服务降级的方法error
    public void error(Cart cart){

    }




    @Override
    public void updateNum(Cart cart) {
        cartMapper.updateCartNumByUserIdAndProductId(cart);
    }

    @Override
    public void deleteCart(Cart cart) {
        cartMapper.deleteCartByUserIdAndProductId(cart);
    }
}
