package cn.jeff.cart.mapper;

import com.jt.common.pojo.Cart;

import java.util.List;

public interface CartMapper {
    List<Cart> selectCartByUserId(String userId);

    Cart selectExistCartByUserIdAndProductId(Cart cart);

    void updateCartNumByUserIdAndProductId(Cart cart);

    void insertCart(Cart cart);

    void deleteCartByUserIdAndProductId(Cart cart);
}
