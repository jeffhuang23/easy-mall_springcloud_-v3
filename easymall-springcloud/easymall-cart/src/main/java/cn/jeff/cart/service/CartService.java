package cn.jeff.cart.service;

import com.jt.common.pojo.Cart;

import java.util.List;

public interface CartService {
    List<Cart> queryMyCarts(String userId);

    void addCart(Cart cart);

    void updateNum(Cart cart);

    void deleteCart(Cart cart);
}
