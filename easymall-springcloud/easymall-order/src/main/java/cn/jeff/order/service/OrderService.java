package cn.jeff.order.service;

import com.jt.common.pojo.Order;

import java.util.List;

public interface OrderService {
    void addOrder(Order order);

    List<Order> queryMyOrders(String userId);

    void deleteOrder(String orderId);
}
