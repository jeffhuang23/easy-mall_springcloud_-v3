package cn.jeff.order.controller;


import cn.jeff.order.mapper.OrderMapper;
import cn.jeff.order.service.OrderService;
import com.jt.common.pojo.Order;
import com.jt.common.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/order/manage")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @RequestMapping("save")
    public SysResult addOrder(Order order){
        /**
         * 订单新增功能
         */
        try{
            orderService.addOrder(order);
            return SysResult.ok();
        }catch (Exception e){
            e.printStackTrace();
            return SysResult.build(201,"新增订单失败",null);
        }
    }


    @RequestMapping("query/{userId}")
    public List<Order> queryMyOrders(@PathVariable String userId){
        /**
         *  订单查询功能
         */
        return orderService.queryMyOrders(userId);
    }

    @RequestMapping("delete/{orderId}")
    /**
     * 删除订单功能
     */
    public SysResult deleteOrder(@PathVariable String orderId){
        try {
            orderService.deleteOrder(orderId);
            return SysResult.ok();
        }catch (Exception e){
            return SysResult.build(201,"删除订单失败",null);
        }
    }

}
