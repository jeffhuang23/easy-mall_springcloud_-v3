package cn.jeff.order.mapper;

import com.jt.common.pojo.Order;

import java.util.List;

public interface OrderMapper {
    void insertOrder(Order order);

    List<Order> selectOrdersByUserId(String userId);

    void deleteOrderByOrderId(String orderId);
}
