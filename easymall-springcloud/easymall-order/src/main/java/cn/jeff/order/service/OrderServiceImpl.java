package cn.jeff.order.service;

import cn.jeff.order.mapper.OrderMapper;
import com.jt.common.pojo.Order;
import com.jt.common.pojo.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired(required = false)
    private OrderMapper orderMapper;
    @Override
    public void addOrder(Order order) {
        //可以在业务层通过循环完成
        /*orderMapper.insertOrder();
        for(OrderItem oi:order.getOrderItems()){
            orderMapper.insertOrderItem(oi);
        }*/
        //也可以使用xml标签完成一次写入一个order对象得功能
        //补充对象的属性 orderId 随机生成
        String orderId = UUID.randomUUID().toString();
        order.setOrderId(orderId);
        order.setOrderTime(new Date());
        order.setOrderPaystate(0);
        orderMapper.insertOrder(order);
    }

    @Override
    public List<Order> queryMyOrders(String userId) {
        return orderMapper.selectOrdersByUserId(userId);
    }

    @Override
    public void deleteOrder(String orderId) {
        orderMapper.deleteOrderByOrderId(orderId);
    }
}
