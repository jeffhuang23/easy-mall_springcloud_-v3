package cn.jeff.service;

import com.jt.common.utils.UUIDUtil;
import com.jt.common.utils.UploadUtil;
import com.jt.common.vo.PicUploadResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.UUID;

@Service
public class PicServiceImpl implements PicService {
    @Override
    public PicUploadResult picUpload(MultipartFile pic) {
        /**
         * 1.验证图片合法, 通过后缀名 .png .jpg .gif 等后缀是合法
         * 2.生成一个多级路径
         *      /upload/a/c/3/d 磁盘路径
         *      url地址
         * 3. 重命名图片名称, 存储到静态文件夹下的路径
         *      d://
         * 4. 对应存储的地址, 生成一个可访问的url
         */
        PicUploadResult result = new PicUploadResult();
        //1.验证图片合法性
        String oname = pic.getOriginalFilename();
        String oxname = oname.substring(oname.lastIndexOf("."));
        if (oxname.matches(".(jpg|png|gif|bmp|jpeg)$")) {
            //合法的就生成一个多级路径
            //单级目录的读取速度没有多级目录查找文件的速度快
            String path = "/" + UploadUtil.getUploadPath(oname, "easymall") + "/";
            String dir =  "D:/Program Files (x86)/nginx-1.9.9/easymall-images" + path;
            //创建磁盘的目录
            File _dir = new File(dir);
            //判断目录是否存在,不存在则创建
            if (!_dir.exists()) {
                //说明不存在,则创建
                _dir.mkdirs();
            }
            //将pic的二进制数据做成一个文件输入到目录
            //直接将pic对象的文件输出到一个磁盘文件中
            //重命名图片名称
            try {
                String pname = UUID.randomUUID().toString() + oxname;
                String url = "http://image.jt.com" + path + pname;
                result.setUrl(url);
                File file = new File(dir + pname);
                pic.transferTo(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;

        } else {
            result.setError(1);
            return result;
        }
    }
}
