package cn.jeff.service;


import com.jt.common.vo.PicUploadResult;
import org.springframework.web.multipart.MultipartFile;

public interface PicService {
    PicUploadResult picUpload(MultipartFile pic);
}
