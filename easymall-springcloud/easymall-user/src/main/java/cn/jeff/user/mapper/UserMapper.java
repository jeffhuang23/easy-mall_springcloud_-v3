package cn.jeff.user.mapper;

import com.jt.common.pojo.User;

public interface UserMapper {
    int selectUserByUserName(String userName);

    void insertUser(User user);

    User selectUserByNameAndPw(User user);
}
