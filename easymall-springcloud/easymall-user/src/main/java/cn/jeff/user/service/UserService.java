package cn.jeff.user.service;


import com.jt.common.pojo.User;

public interface UserService {

    Boolean checkUserName(String userName);

    void doRegister(User user);

    String doLogin(User user);

    String queryTicket(String ticket);
}
