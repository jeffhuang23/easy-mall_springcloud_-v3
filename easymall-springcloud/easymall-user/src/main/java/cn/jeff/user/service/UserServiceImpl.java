package cn.jeff.user.service;

import cn.jeff.user.mapper.UserMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.common.pojo.User;
import com.jt.common.utils.MD5Util;
import org.apache.commons.codec.digest.Md5Crypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    private UserMapper userMapper;
    @Override
    public Boolean checkUserName(String userName) {
        /**
         * 用户名校验是否存在
         */
        int num = userMapper.selectUserByUserName(userName);
        return num == 0;
    }

    @Override
    public void doRegister(User user) {
        /**
         * 用户注册
         */
        //user补充userId
        String userId = UUID.randomUUID().toString();
        user.setUserId(userId);
        //加密MD5铭文加密成密文, 无法反向推算明文
        //利用记录所有MD5中明文对应密文的表格
        //在MD5 加密时防止通过表格对应关系硬破解---加盐
        //密码中加入一段随机字符串, 再进行加密
//        String saltPassword = password + "随机字符串";
        String MD5password = MD5Util.md5(user.getUserPassword());
        user.setUserPassword(MD5password);
        userMapper.insertUser(user);
    }

    private ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private StringRedisTemplate template;
    /**
     * 用户登录功能
     *
     * 对user明文密码加密处理
     * 到数据库查询
     * 判断查询结果user是否为空,为空说明登录失败, 不为空则登录成功
     * 将用户信息存储到redis 中 username 作为key
     * value为user对象转化的userJson字符串
     * key值体现: 用一个用户不同时间登录, ticket生成不同的值, 不同用户登录总是不同ticket
     *        如果用拼接UU`ID来区分不同用户, 不能体现用户逻辑
     *        key = 前缀EM_TICKET_ + 用户名 + 登录时间 currentTime
     *  存储key-value--- ticket-userJson 保证一个超时管理2小时
     */
   /*
    @Override
    public String doLogin(User user) {
        *//*
        //明文密码
        user.setUserPassword((MD5Util.md5(user.getUserPassword())));
        //校验查询
        User existUser = userMapper.selectUserByNameAndPw(user);
        //判断是否为null
        if (existUser == null){
            return "";
        }else {
            //校验成功存储redis
            //为每个用户拼接ticket字符串
            String ticket = "EM_TICKET_" + user.getUserName() + System.currentTimeMillis();
            //value中保存existUser 对象的json字符串 ,使用jackson中代码
            try{
                //将对象中密码删除, 缓存中不保存密码(安全考虑)
                existUser.setUserPassword("");
                //序列化, 将对象转化为json字符串
                String userJson = objectMapper.writeValueAsString(existUser);
                template.opsForValue().set(ticket,userJson,2, TimeUnit.HOURS);
                return ticket;
            }catch (Exception e){
               e.printStackTrace();
               return "";
            }

        }
    }*/

    @Override
    public String doLogin(User user) {
        /**
         *  将用户顶替的逻辑加入登录功能
         */
        user.setUserPassword(MD5Util.md5(user.getUserPassword()));
        User existUser = userMapper.selectUserByNameAndPw(user);

        if (existUser == null || "".equals(existUser)) {
            return "";
        } else {
            String newTicket = "EM_TICKET_" + user.getUserName() + System.currentTimeMillis();
            //生成一个userName有关的key
            String userLoginKey = "user_login_" + user.getUserName();
            //判断userName的key---userLoginKey是否存在, 存在则说明已经有用户在登录状态
            if (template.hasKey(userLoginKey)) {
                //获取上次的ticket
                String oldTicket = template.opsForValue().get(userLoginKey);
                template.delete(oldTicket);
            }
            try {
                existUser.setUserPassword("");
                String userJson = objectMapper.writeValueAsString(existUser);
                template.opsForValue().set(newTicket, userJson, 2, TimeUnit.HOURS);
                //在生成一个新的用户名和ticket的key-value
                template.opsForValue().set(userLoginKey, newTicket,2,TimeUnit.HOURS);
                return newTicket;
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    @Override
    public String queryTicket(String ticket) {
        /**
         * 用户登录后保持登录状态
         * 超时续租
         */
        //在redis中根据key值找到value
    /*
      return template.opsForValue().get(ticket);
     */

        //加入超时续租逻辑
       Long leftTime = template.getExpire(ticket,TimeUnit.SECONDS);
       //正整数, 对一个超时数据设置重新超时2小时不影响删除的情况
        if(leftTime >0 && leftTime < 60*60){
            //正在使用的状态, 已经使用超过1小时
            //重新续租, 重新设置超时时间
            template.expire(ticket,60*60*2,TimeUnit.SECONDS);
            //substring 左闭右开
            String userName = ticket.substring(10, (ticket.length() - 13));
            String userLoginKey = "user_login_" + userName;
            //userLoginKey记录唯一有效ticket也要重新设置
            template.expire(userLoginKey,60*60*2, TimeUnit.SECONDS);
        }
        return template.opsForValue().get(ticket);
    }


}
