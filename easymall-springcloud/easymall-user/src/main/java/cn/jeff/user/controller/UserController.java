package cn.jeff.user.controller;

import cn.jeff.user.service.UserService;
import com.jt.common.pojo.User;
import com.jt.common.utils.CookieUtils;
import com.jt.common.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("user/manage/")
public class UserController {
    @Autowired
    private UserService userService;
    //注册功能的用户名校验
    @RequestMapping("checkUserName")
    public SysResult checkUserName(String userName){
        //接收业务层返回的boolean值, true为可用, false为不可用
        Boolean check = userService.checkUserName(userName);
        if (!check){
            return SysResult.build(201, "用户已存在",null);
        }else {
            return SysResult.ok();
        }
    }

    /**
     * 注册表单提交
     */
    @RequestMapping("save")
    public SysResult doRegister(User user){
        try{
            userService.doRegister(user);
            return SysResult.ok();
        }catch (Exception e){
            e.printStackTrace();
            return SysResult.build(201,"注册用户失败",null);
        }
    }

    /**
     * 用户登录功能
     */
    @RequestMapping("login")
    public SysResult doLogin(User user, HttpServletRequest request, HttpServletResponse response){
        //通过业务层判断登录校验结果, 如果成功则写入redis,返回redis的key
        //这个key设为ticket, 如果ticket为空 或null , 说明登录校验失败, 不为空则成功
        String ticket = userService.doLogin(user);
        if(ticket == null || "".equals(ticket)){
            //redis没有存储当前登录数据,登录校验也是失败的
            return SysResult.build(201,"登录失败",null);
        }else {
            /*Cookie cookie = new Cookie("EM_TICKET", ticket);
            response.addCookie(cookie);
            利用servlet技术将cookie加入response响应回浏览器
            */
            //利用封装好的工具类将cookie加入response中
            CookieUtils.setCookie(request,response,"EM_TICKET",ticket);
            return SysResult.ok();
        }
    }

    @RequestMapping("query/{ticket}")
    public SysResult queryTicket(@PathVariable String ticket){
        /**
         * 用户登录状态的获取
         */
        //可能超时
        String userJson = userService.queryTicket(ticket);
        if(userJson == null || "".equals(userJson)){
            return SysResult.build(201,"数据超时",null);
        }else {
            //登录状态正常
            return SysResult.build(200,"ok",userJson);
        }
    }

    @RequestMapping("logout")
    public SysResult doLogout(HttpServletRequest request, HttpServletResponse response){
        /**
         * 用户登出功能
         * 将cookie中EM_TICKET删除
         */
        String ticket = CookieUtils.getCookieValue(request, "EM_TICKET");
        //如果存在EM_TICKET的cookie , 证明有登录状态
        if (ticket == null || "".equals(ticket)){
            return SysResult.ok();
        }else {
            try{
                CookieUtils.setCookie(request,response, "EM_TICKET","");
                return SysResult.ok();
            }catch (Exception e){
                e.printStackTrace();
                return SysResult.build(201,"登出失败",null);
            }
        }
    }
}
