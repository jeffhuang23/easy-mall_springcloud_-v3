import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.common.pojo.User;
import com.jt.common.utils.MD5Util;

import java.io.IOException;

public class jacksonDemo {
    /**
     * jackson 的序列化和反序列
     */
    public static void main(String[] args) throws IOException {
        User user = new User();
        user.setUserPassword("12345");
        user.setUserId("zhangsan");
        user.setUserEmail("12@163.com");
        user.setUserNickname("112");
        user.setUserType(0);

        ObjectMapper mapper = new ObjectMapper();
        String userJson = mapper.writeValueAsString(user);
        System.out.println(userJson);
        User user1 = mapper.readValue(userJson, User.class);
        System.out.println(user1);
    }
}
