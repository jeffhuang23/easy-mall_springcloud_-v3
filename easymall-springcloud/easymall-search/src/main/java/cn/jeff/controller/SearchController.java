package cn.jeff.controller;

import cn.jeff.service.SearchService;
import com.jt.common.pojo.Product;
import org.elasticsearch.index.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class SearchController {
    @Autowired
    private SearchService searchService;
    @RequestMapping("/search/manage/query")
    public List<Product> search(@RequestParam("query") String text, Integer page,Integer rows) throws IOException {
        return searchService.search(text,page,rows);
    }
}
