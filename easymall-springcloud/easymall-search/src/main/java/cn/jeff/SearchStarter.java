package cn.jeff;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@MapperScan("cn.jeff.mapper")
@EnableEurekaClient
public class SearchStarter {
    public static void main(String[] args) {
        SpringApplication.run(SearchStarter.class,args);
    }

    @Bean
    public ObjectMapper init(){
        return new ObjectMapper();
    }
}
