package cn.jeff;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

/**
 * 通过Configuration注解, 将该类作为配置类进行加载
 * 配置类的作用就是生成一个容器管理的对象TransportClient
 */
@Configuration
@ConfigurationProperties("easymall.es") //该注解可以读取properties文件中的以easymall.es 开头的配置, 通过自定义前缀, 可以多级赋值
//@Value 作用一样可以读取properties
public class ESConfig {
    private String name;

    //创建对象时, 需要ip,port的es节点集群
    // 将properties文件中配置 easymall.es.nodes=es01,es02,es03  赋值给nodes
    private List<String> nodes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getNodes() {
        return nodes;
    }

    public void setNodes(List<String> nodes) {
        this.nodes = nodes;
    }

    @Bean
    public TransportClient initClient(){
        PreBuiltTransportClient client =
                new PreBuiltTransportClient(Settings.EMPTY);
        InetSocketTransportAddress address;
        //利用nodes 获取连接对象ip: port
        for (String node: nodes) {
            String[] nodeInfo = node.split(":");
            String host = nodeInfo[0];
            int port = Integer.parseInt(nodeInfo[1]);
            try {
                address =
                        new InetSocketTransportAddress(InetAddress.getByName(host),port);
            } catch (UnknownHostException e) {
                e.printStackTrace();
                continue;
            }
            //添加到client对象中
            client.addTransportAddress(address);
        }
        return client;
    }

}


