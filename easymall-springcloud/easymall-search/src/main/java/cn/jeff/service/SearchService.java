package cn.jeff.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.common.pojo.Product;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class SearchService {
    @Autowired
    private TransportClient client;
    @Autowired
    private ObjectMapper mapper;
    public List<Product> search(String text, Integer page, Integer rows) throws IOException {
        //封装一个query对象
        MatchQueryBuilder query = QueryBuilders.matchQuery("productName", text);

        //搜索分页逻辑
        int from = (page -1)*rows;
        int size = rows;
        SearchRequestBuilder request =
                client.prepareSearch("easymall").setQuery(query).setFrom(from).setSize(size);
        SearchResponse searchResponse = request.get();
        //从response中解析数据, 封装返回结果
        ArrayList<Product> pList = new ArrayList<Product>();
        SearchHit[] hits = searchResponse.getHits().getHits();
        for (SearchHit hit : hits) {
            //将搜索出来的信息封装成Product对象
            Product product = mapper.readValue(hit.getSourceAsString(), Product.class);
            pList.add(product);
        }
        return pList;
    }
}
