package cn.jeff.product.controller;

import cn.jeff.product.service.ProductService;
import com.jt.common.pojo.Product;
import com.jt.common.vo.EasyUIResult;
import com.jt.common.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("product/manage")
public class ProductController {
    @Autowired
    private ProductService productService;

    @RequestMapping("/pageManage")
    public EasyUIResult queryProductsByPage(Integer page, Integer rows){
        /**
         *  商品分页查询
         */
        //page是需要查询的页码, rows 是每页条数, 调用业务层实现EasyUIResult 对象封装
        EasyUIResult result = productService.queryProductsByPage(page,rows);
        return result;
    }

    @RequestMapping("item/{productId}")
    public Product queryOneProduct(@PathVariable String productId){
        /**
         * 根据商品id值查询单个商品
         */
        //RestFUL 风格路径参数获取
        //如果有多个需要接受的参数@PathVariable注解, 要指明具体的参数
        //@PathVariable(name= "productId")
        return productService.queryOneProduct(productId);
    }


    @RequestMapping("/save")
    //请求参数productCategory=**&productName
    public SysResult addProduct(Product product){
        /**
         * 商品新增
         */
        try{
            productService.addProduct(product);
            return SysResult.ok(); //自定义的静态方法,其中参数已经进行封装
                                    //{"status": 200,"masg":"ok","data":null}
        }catch (Exception e){
            e.printStackTrace();
            return SysResult.build(201,"新增商品失败",null);
        }
    }

    @RequestMapping("/update")
    public SysResult editProduct(Product product){
        /**
         * 商品更新数据库
         */
        try{
            productService.editProduct(product);
            return SysResult.ok();
        }catch (Exception e){
            e.printStackTrace();
            return new SysResult(201, "更新失败", null);
        }
    }

}
