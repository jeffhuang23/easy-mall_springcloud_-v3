package cn.jeff.product.mapper;


import com.jt.common.pojo.Product;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductMapper {
    int selectCount();
    //mybatis对参数超过2个, 必须使用mybatis注解, 否则无法对应sql 语句中预编译的参数
    //也可以使用将参数封装进对象传入
    List<Product> selectProductByPage(@Param("start") Integer start, @Param("rows") Integer rows);

    Product selectProductByProductId(String productId);

    void insertProduct(Product product);

    void updateProductById(Product product);
}
