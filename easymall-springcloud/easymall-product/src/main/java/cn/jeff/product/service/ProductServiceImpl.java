package cn.jeff.product.service;

import cn.jeff.product.mapper.ProductMapper;
import cn.jeff.product.utils.ESUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.common.pojo.Product;
import com.jt.common.vo.EasyUIResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private StringRedisTemplate template;
    @Autowired(required = false)
    private ProductMapper productMapper;

    @Override
    public EasyUIResult queryProductsByPage(Integer page, Integer rows) {
        //封装一个EasyUIResult对象
        EasyUIResult result = new EasyUIResult();
        //total 查询总数据
        //List<Product> rows 类型分页查询结果
        int total = productMapper.selectCount();
        //利用page计算查询start的起始位置
        int start = (page - 1) * rows;
        List<Product> plist = productMapper.selectProductByPage(start, rows);
        //使用page, rows 的参数  将分页数据查询结构封装
        result.setRows(plist);
        return result;
    }

    //将对象封装成json字符串的类
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public Product queryOneProduct(String productId) {
        /**
         * 查询单个商品信息
         * 被动缓存    ----加入redis的缓存逻辑
         * 判断缓存中有没有当前商品的数据key-value  有则直接使用,没有到数据库查询
         * 存放缓存一份
         * 加入内存锁保证数据一致性
         */
        //判断锁是否存在
        String lock = "product_" + productId + ".lock";
        if (template.hasKey(lock)) {
            //如果锁存在, 说明商品正在更新, 则直接从数据库的数据获取数据(是更新前的数据)
            return productMapper.selectProductByProductId(productId);
        }

        //被动缓存
        String productKey = "product_" + productId;
        if (template.hasKey(productKey)) {
            //获取缓存的json, 反序列化成product对象
            String pJson = template.opsForValue().get(productKey);
            try {
                //将获取的json字符串反序列化成对象返回
                return mapper.readValue(pJson, Product.class);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            //缓存没有数据, 查询数据库
            Product product = productMapper.selectProductByProductId(productId);
            try {
                //存放到缓存一份, key=productKey value=pJson
                String pJson = mapper.writeValueAsString(product);
                template.opsForValue().set(productKey, pJson, 2, TimeUnit.DAYS);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return product;
        }
//        没有加入redis缓存则直接到数据库中查询
//        return productMapper.selectProductByProductId(productId);

    }


    /**
     * 在增加商品功能中加入缓存和索引逻辑
     */
    @Autowired
    private ESUtils esUtils;
    @Override
    public void addProduct(Product product) {
        //商品数据补齐, 缺少必要属性productId
        String productId = UUID.randomUUID().toString();
        product.setProductId(productId);
        // 加入主动缓存逻辑
        // 将添加的商品在缓存中存入一份
        String productKey = "product_" + productId;
        try {
            String pJson = mapper.writeValueAsString(product);
            //将数据存入缓存
            template.opsForValue().set(productKey, pJson, 2, TimeUnit.DAYS);
            //再将数据存入数据库
            productMapper.insertProduct(product);
            //将单个新增加的商品加入索引文件
            if (!esUtils.isIndexExsits("easymall")){
                esUtils.createIndexByName("easymall");
            }
            esUtils.createDoc("easymall","product",productId,pJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void editProduct(Product product) {
        /**
         * 保证数据一致性, 同步执行
         * 通过对添加一个内存锁, 只有发现内存锁的存在, 则说明正在更新数据
         */
        String productKey = "product_" + product.getProductId();
        //锁的key值
        String lock = "product_" + product.getProductId() + ".lock";
        //将入该商品的锁
        Boolean ok = template.opsForValue().setIfAbsent(lock, "");
        template.expire(lock, 5, TimeUnit.HOURS);
        if (ok) {  //设置锁成功
            //删除旧缓存
            template.delete(productKey);
            //执行数据库更新
            productMapper.updateProductById(product);
        } else {
            throw new RuntimeException("更新缓存发现有冲突, 有人正在更新");
        }
        //释放锁
        template.delete(lock);
        //将修改后的product 数据覆盖原有的document
        try {
            String pJson = mapper.writeValueAsString(product);
            esUtils.createDoc("easymall","product",product.getProductId(),pJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
