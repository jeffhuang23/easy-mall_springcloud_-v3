package cn.jeff.product.controller;


import cn.jeff.product.service.IndexService;
import com.jt.common.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @Autowired
    private IndexService indexService;
    //创建索引文件
    @RequestMapping("/createIndex")
    public SysResult createIndex(String indexName, String type){
       try{
           indexService.createIndex(indexName,type);
           return SysResult.ok();
       }catch (Exception e){
           e.printStackTrace();
           return SysResult.build(201,"索引创建失败",null);
       }
    }
}
