package cn.jeff.product.utils;


import org.elasticsearch.client.transport.TransportClient;
import org.springframework.stereotype.Component;

@Component
public class ESUtils {
    private TransportClient client;

    public boolean isIndexExsits(String indexName) {
        return client.admin().indices().prepareExists(indexName).get().isExists();
    }

    public boolean createIndexByName(String indexName) {
        return client.admin().indices().prepareCreate(indexName).get().isAcknowledged();
    }

    public void createDoc(String indexName, String type, String id, String json) {
        client.prepareIndex(indexName, type, id).setSource(json).get();
    }
}
