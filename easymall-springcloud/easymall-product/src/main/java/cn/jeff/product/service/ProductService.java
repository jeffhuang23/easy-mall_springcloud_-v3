package cn.jeff.product.service;

import com.jt.common.pojo.Product;
import com.jt.common.vo.EasyUIResult;

public interface ProductService {
    EasyUIResult queryProductsByPage(Integer page, Integer rows);

    Product queryOneProduct(String productId);

    void addProduct(Product product);

    void editProduct(Product product);
}
