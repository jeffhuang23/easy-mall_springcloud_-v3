package cn.jeff.product.service;

import cn.jeff.product.mapper.ProductMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.common.pojo.Product;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexService {
    @Autowired
    private ObjectMapper objectMapper ;
    @Autowired
    private TransportClient client;
    @Autowired(required = false)
    private ProductMapper mapper;

    public void createIndex(String indexName, String type) throws JsonProcessingException {
        //拿到集群索引管理对象
        IndicesAdminClient indices = client.admin().indices();
        //判断这个索引是否存在
        IndicesExistsResponse indicesExistsResponse = indices.prepareExists(indexName).get();
        if (!indicesExistsResponse.isExists() ){
            indices.prepareCreate(indexName).get();
        }
        List<Product> products = mapper.selectProductByPage(0,100);
        for (Product p : products) {
            String pJson = objectMapper.writeValueAsString(p);
            client.prepareIndex(indexName,type,p.getProductId()).setSource(pJson).get();
        }
    }
}
