package cn.jeff;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("cn.jeff.product.mapper")
public class StarterProduct {

    public static void main(String[] args) {
        SpringApplication.run(StarterProduct.class, args);
    }
    @Bean
    public ObjectMapper init(){
        return new ObjectMapper();
    }
}
