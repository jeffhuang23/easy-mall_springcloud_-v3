package cn.jeff.seckill.service;

import com.jt.common.pojo.Seckill;
import com.jt.common.pojo.Success;

import java.util.List;

public interface SeckillService {
    List<Seckill> selectSeckill();

    Seckill selectSeckillDetail(Long seckillId);

    List<Success> querySuccess(Long seckillId);
}
