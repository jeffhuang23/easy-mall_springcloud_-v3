package cn.jeff.seckill;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
@EnableEurekaClient
@MapperScan("cn.jeff.seckill.mapper")
public class StarterSeckill {
    public static void main(String[] args) {
        SpringApplication.run(StarterSeckill.class,args);
    }

  @Bean
  public Queue queue01(){
        return new Queue("seckillQ",false,false,false,null);
  }

  @Bean
  public DirectExchange ex01(){
        return new DirectExchange("seckillEX");
  }

  @Bean
  public Binding bind01(){
        return BindingBuilder.bind(queue01()).to(ex01()).with("seckill");
  }

}

