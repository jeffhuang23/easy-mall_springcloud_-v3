package cn.jeff.seckill.mapper;

import com.jt.common.pojo.Seckill;
import com.jt.common.pojo.Success;

import java.util.List;

public interface SeckillMapper {
    List<Seckill> selectAllSeckill();

    Seckill selectSeckillById(Long seckillId);

    int decrNumberById(Long seckillId);

    void insertSuccess(Success suc);

    List<Success> selectSuccessById(Long seckillId);
}
