package cn.jeff.seckill.service;

import cn.jeff.seckill.mapper.SeckillMapper;
import com.jt.common.pojo.Seckill;
import com.jt.common.pojo.Success;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeckillServiceImpl implements SeckillService {
    @Autowired
    private SeckillMapper mapper;
    @Override
    public List<Seckill> selectSeckill() {

        return mapper.selectAllSeckill();
    }

    @Override
    public Seckill selectSeckillDetail(Long seckillId) {
        return mapper.selectSeckillById(seckillId);
    }

    @Override
    public List<Success> querySuccess(Long seckillId) {
        return mapper.selectSuccessById(seckillId);
    }
}
