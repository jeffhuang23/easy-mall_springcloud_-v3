package cn.jeff.seckill.Consumer;

import cn.jeff.seckill.mapper.SeckillMapper;
import com.jt.common.pojo.Success;
import com.jt.common.vo.SysResult;
import org.omg.PortableInterceptor.SUCCESSFUL;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class SeckillConsumer {
    /**
     * 编写消费逻辑
     */

    @Autowired(required = false)
    private SeckillMapper seckillMapper;
    @Autowired
    private StringRedisTemplate template;
    @RabbitListener(queues = "seckillQ")
    public void consume(String msg){
        //msg = 电话号码/seckillId
        /**
         *  userPhone seckillId 解析
         *  根据seckillId  执行库存
         *  number>0
         *  时间 > start_time
         *  < end_time
         *  打印谁抢到了商品
         *  收集信息, 入库success 表, 表示成功秒杀
         */
        String[] msgs = msg.split("/");
        String userName = msgs[0];
        Long userPhone = Long.parseLong(msgs[1]);
        Long seckillId = Long.parseLong(msgs[2]);
        //执行减库存, 在redis中找到对应的商品, 将数量减少
        if (template.hasKey(userName)){
            System.out.println("你刚刚已经秒杀过了");
            return;
        }
        Long decr = template.opsForValue().increment("num_"+msgs[2], -1);
        if (decr < 0){
            //如果没有库存了, redis返回-1
            System.out.println("商品库存没有了");
            return;
        }
        int result = seckillMapper.decrNumberById(seckillId);
        if(result == 0){ //如果减库存失败
            System.out.println("用户" + userPhone + "秒杀失败");
            return;
        }
        //秒杀成功后, 将用户信息写入数据库
        Success suc = new Success();
        suc.setUserPhone(userPhone);
        suc.setCreateTime(new Date());
        suc.setSeckillId(seckillId);
        suc.setState(0);
        //秒杀成功的用户,写入redis , 5分钟后才能再次秒杀
        template.opsForValue().set(userName,"killed",5, TimeUnit.MINUTES);
        System.out.println(userName+ "秒杀成功");
        seckillMapper.insertSuccess(suc);
    }
}
