package cn.jeff.seckill.controller;

import cn.jeff.seckill.service.SeckillService;
import com.jt.common.pojo.Seckill;
import com.jt.common.pojo.Success;
import com.jt.common.utils.CookieUtils;
import com.jt.common.vo.SysResult;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/seckill/manage/")
public class SeckillController {
    @Autowired
    private SeckillService seckillService;
    @RequestMapping("list")
    public List<Seckill> list(){
        return seckillService.selectSeckill();
    }


    @RequestMapping("detail")
    public Seckill detail(Long seckillId){
        return seckillService.selectSeckillDetail(seckillId);
    }


    @Autowired
    private RabbitTemplate rabbitTemplate;
    @RequestMapping("/{seckillId}")
    /**
     * 生产端, 发送用户秒杀的信息
     */
    public SysResult startSeckill(@PathVariable("seckillId") Long seckillId, String userName){
        //模拟每次不同用户访问商品
        //TODO 功能: 如果只允许一个用户秒杀一次, 使用redis
        if (userName == null){
            return SysResult.build(202,"没有登录",null);
        }
        String userPhone = "1234567" + new Random().nextInt(9999);
        String msg = userName+ "/" + userPhone + "/" + seckillId;
        try{
            rabbitTemplate.convertAndSend("seckillEX","seckill", msg);
            return SysResult.ok();
        }catch (Exception e){
            e.printStackTrace();
            return SysResult.build(201,"秒杀失败",null);
        }
        //等待声明代码配置完毕再验证功能
    }

    @RequestMapping("{seckillId}/userPhone")
    public List<Success> seckillInfo(@PathVariable Long seckillId, HttpServletRequest request, HttpServletResponse response){
        /**
         * 秒杀成功后展示成功信息
         */
        Seckill seckill = seckillService.selectSeckillDetail(seckillId);
        String sellPoint = seckill.getSellPoint();
        CookieUtils.setCookie(request,response,"sellPoint",sellPoint);
        return seckillService.querySuccess(seckillId);
    }
}
