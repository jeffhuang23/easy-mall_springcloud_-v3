package cn.jeff.product;

import com.jt.common.pojo.Product;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "ProductService")
public interface ProductApi {
    @RequestMapping(value = "/product/manage/item/{productId}",method = RequestMethod.GET)
    Product queryProduct(@PathVariable("productId") String productId);
}
