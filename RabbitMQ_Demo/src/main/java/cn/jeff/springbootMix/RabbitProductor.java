package cn.jeff.springbootMix;

import org.junit.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RabbitProductor {
    @Autowired
    private RabbitTemplate template;
    @RequestMapping("/send")
    public String send(String msg){
        //发送msg 到队列
        //自定义消息属性设置
        MessageProperties properties = new MessageProperties();
        properties.setPriority(100);
        Message message = new Message(msg.getBytes(), properties);
        template.send("topicEX","中国.北京.成都",message);
        //自动将字符串封装成消息
        template.convertAndSend("topicEX","成都",msg);
        return "success";
    }
}
