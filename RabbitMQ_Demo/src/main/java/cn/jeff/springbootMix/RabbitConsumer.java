package cn.jeff.springbootMix;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListeners;
import org.springframework.stereotype.Component;

@Component
public class RabbitConsumer {
    @RabbitListener(queues = {"topicqueue01","topicqueue02"})
    public void consume(String msg){
        //当前方法就是消费端调用的消费逻辑
        //底层链接拿到消息之后会传递给方法的参数
        System.out.println("消费端接收消息: "+ msg);
    }
}
