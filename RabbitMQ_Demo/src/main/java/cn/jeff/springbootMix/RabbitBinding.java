package cn.jeff.springbootMix;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RabbitBinding {
    @Bean
    //通过Bean管理一个队列对象
    public Queue queue01(){
        return new Queue("queue01",false,false,false,null);
    }
    @Bean
    //管理一个交换机对象
    public DirectExchange ex01(){
        return new DirectExchange("direct",false,false,null);
    }
    @Bean
    public Binding binding01(){
        return BindingBuilder.bind(queue01()).to(ex01()).with("成都");
    }
}
