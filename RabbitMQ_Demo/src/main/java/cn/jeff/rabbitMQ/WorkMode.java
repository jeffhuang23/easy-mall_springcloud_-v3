package cn.jeff.rabbitMQ;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

public class WorkMode {
    //创建连接对象(短连接)
    private Channel channel;
    @Before
    public void initChannel() throws IOException, TimeoutException {
        //提供一些了解参数 host port  user  password
        //创建一个连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        //提供连接属性
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setHost("10.42.12.113");
        connectionFactory.setPort(5672);
        //获取连接对象(长链接)
        Connection connection = connectionFactory.newConnection();
        //从长连接获取短连接
        //从连接对象获取channel 赋值给私有属性
        channel = connection.createChannel();
    }

    @Test
    public void productor() throws IOException {
        String msg = "workmode----rabbitmq";

        channel.queueDeclare("workqueue",false,false,false,null);

//        channel.basicPublish("","workqueue",null,msg.getBytes());
        //批量发送
        for (int i = 0; i < 10; i++) {
            channel.basicPublish("","workqueue",null,(i + ":" + msg).getBytes());
        }
    }

    @Test
    public void consumer01() throws InterruptedException, IOException {
        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume("workqueue",true,consumer);

        while (true){
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            byte[] body = delivery.getBody();
            System.out.println("消费者01: "+ new String(body,"utf-8"));
        }
    }

    @Test
    public void consumer02() throws InterruptedException, IOException {
        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume("workqueue",true,consumer);

        while (true){
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            byte[] body = delivery.getBody();
            System.out.println("消费者02: "+ new String(body,"utf-8"));
        }
    }
}
