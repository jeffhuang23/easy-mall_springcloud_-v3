package cn.jeff.rabbitMQ;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class FanOutMode {
    /**
     * 订阅模式
     */
    //创建连接对象(短连接)
    private Channel channel;
    @Before
    public void initChannel() throws IOException, TimeoutException {
        //提供一些了解参数 host port  user  password
        //创建一个连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        //提供连接属性
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setHost("10.42.12.113");
        connectionFactory.setPort(5672);
        //获取连接对象(长链接)
        Connection connection = connectionFactory.newConnection();
        //从长连接获取短连接
        //从连接对象获取channel 赋值给私有属性
        channel = connection.createChannel();
    }

    //定义配置的静态常亮
    private static final String type = "fanout"; //自定义交换机
    private static final String exName = type + "EX";//自定义交换机名称
    //准备2个队列
    private static final String q1 = type + "queue01";
    private static final String q2 = type + "queue02";

    @Test
    public void bind() throws IOException {
        //声明队列
        channel.queueDeclare(q1,false,false,false,null);
        channel.queueDeclare(q2,false,false,false,null);
        // 声明交换机
        channel.exchangeDeclare(exName,type);
        //声明绑定关系
        //交换机绑定交换机    用于交换机之间的通信
//        channel.exchangeBind("")
        //队列绑定交换机, 只要发送到指定的交换机, 就会发送到该交换机的所有队列
        channel.queueBind(q1,exName,"成都");
        channel.queueBind(q1,exName,"上海");
        channel.queueBind(q2,exName,"北京");
    }
    @Test
    public void productor() throws IOException {
        //将消息进行群发
        channel.basicPublish(exName,"上海",null,"来了不走".getBytes());
        channel.basicPublish(exName,"上海",null,"大上海".getBytes());

    }
}
