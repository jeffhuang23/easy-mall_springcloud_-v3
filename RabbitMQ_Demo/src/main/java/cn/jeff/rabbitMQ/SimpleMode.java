package cn.jeff.rabbitMQ;


import com.rabbitmq.client.*;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class SimpleMode {
    /**
     *
     */
    //创建连接对象(短连接)
    private Channel channel;
    @Before
    public void initChannel() throws IOException, TimeoutException {
        //提供一些了解参数 host port  user  password
        //创建一个连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        //提供连接属性
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setHost("10.42.12.113");
        connectionFactory.setPort(5672);
        //获取连接对象(长链接)
        Connection connection = connectionFactory.newConnection();
        //从长连接获取短连接
        //从连接对象获取channel 赋值给私有属性
        channel = connection.createChannel();
    }

    @Test
    public void productor() throws IOException {
        /**
         * 通过channel 操作rabbitmq, 代表客户端实现消息的发送
         */
        String msg = "hello world, rabbitmq";
        //声明一个队列
        channel.queueDeclare("queue01",false,false,false, null);
        /**
         * @queue 声明这个队列的名字,  如果存在
         * @durable  boolean   表示队列是否持久化, 持久化队列会在MQ宕机重启后恢复
         * @exclusive boolean  表示队列是否专属于当前连接, true 表示专属, 除了创建声明的连接可以操作, 其他连接不能操作
         * @autoDelete  boolean  最后一个channel连接queue结束后自动删除queue
         * @args   map   表示队列的属性设置
         *      x_message_ttl 消息最长存活时间
         */
        channel.basicPublish("","queue01",null,msg.getBytes());
        /**
         * @exchange  交换机名称  ""空字符串发送给默认交换机
         * @routingKey  当前消息发送时绑定的路由key
         * @BasicProperites  props  封装消息对象message中的属性,可以丰富消息的信息, 更方便处理消费的逻辑
         *      priority  优先级 content_encoding 内容编码集  correlation_id 消息之间的关系  expiration 超时
         *      timestamp  时间戳
         * @body  消息体, 以byte[] 形式发送
         *  由于消息数据占用网络带宽传输, 存储在queue中一段时间, 消息的封装按照精简准确
         */
    }

    @Test
    public void consumer01() throws IOException, InterruptedException {
        /**
         * 消费端逻辑
         * 通过channel 信道连接rabbitmq,监听任何一个或多个队列
         *
         */
        QueueingConsumer queueingConsumer = new QueueingConsumer(channel);
        //将consumer 绑定监听队列
//        channel.basicConsume("queue01",queueingConsumer);
        // 消费端绑定了队列后, 设置了autoAck, 消费端拿了消息后,队列会删除消息
        channel.basicConsume("queue01",false,queueingConsumer);
        //通过链接调用消息,  只调用了一次
        QueueingConsumer.Delivery delivery = queueingConsumer.nextDelivery();
        //delivery是一个包含消息的对象
        byte[] body = delivery.getBody();
        AMQP.BasicProperties properties = delivery.getProperties();
        Envelope envelope = delivery.getEnvelope();
        System.out.println(new String(body,"utf-8"));
        System.out.println( envelope.getRoutingKey() );

        //手动确认, multiple 表示是否多条确认
        channel.basicAck(envelope.getDeliveryTag(),false);
    }

}
