package cn.jeff;

import org.springframework.stereotype.Component;

@Component
public class FeignServiceFallBack implements FeignService{
    @Override
    public String say(String name) {
        return "say it slowly";
    }
}
