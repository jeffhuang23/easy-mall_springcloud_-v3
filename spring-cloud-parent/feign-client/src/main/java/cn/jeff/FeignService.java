package cn.jeff;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.FeignClientsConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "service-hi",fallback = FeignServiceFallBack.class)
public interface FeignService {
    //代理实现微服务调用提供service-hi
    //http://service-hi/client/hello
    //抽象方法编写了springmvc的注解实现底层需要的参数
    @RequestMapping(value = "/client/hello",method = RequestMethod.GET)
    String say(@RequestParam("name") String name);
}
