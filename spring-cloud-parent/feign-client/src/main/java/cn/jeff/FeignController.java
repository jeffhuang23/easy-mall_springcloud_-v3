package cn.jeff;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignController {
    @Autowired(required = false)
    private FeignService feignService;
    @RequestMapping("feign/hello")
    public String sayHi(String name){
        return "feign: " + feignService.say(name);
    }
}
