package cn.jeff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy //开启zuul 的代理, 目的引入zuul中的过滤逻辑(网关匹配,转发路由)
public class StarterGateWay {
    public static void main(String[] args) {
        SpringApplication.run(StarterGateWay.class, args);
    }
}
