package cn.jeff;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 自定义网关过滤器
 */
@Component
public class MyZuulFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        //只有uri 满足 以/zuul-a/** 开始的才会拦截, 去执行后面的run拦截逻辑
        //需要通过zuul中的请求链上下文对象获取当前请求
        RequestContext context = RequestContext.getCurrentContext();
        HttpServletRequest request = context.getRequest();
        //从请求中获取uri 的信息
        String requestURI = request.getRequestURI();
        return requestURI.startsWith("/zuul-a/");
    }

    @Override
    public Object run() {
        //过滤的核心逻辑
        RequestContext context = RequestContext.getCurrentContext();
        HttpServletRequest request = context.getRequest();
        HttpServletResponse response = context.getResponse();
        //从request获取参数
        String name = request.getParameter("name");
        if (name == null) {
            //请求中没有叫name的参数
            //进行拦截
            context.setSendZuulResponse(false); //请求是否响应
            context.setResponseStatusCode(403);
            //手动拼接响应体
            response.setContentType("text/html;charset=utf-8");
            context.setResponseBody("{\"status\": \"201\",\"msg\":\"参数格式错误\"}");
        }
        return null;
    }
}
