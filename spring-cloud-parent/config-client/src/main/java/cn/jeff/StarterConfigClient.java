package cn.jeff;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class StarterConfigClient {
    public static void main(String[] args) {
        SpringApplication.run(StarterConfigClient.class,args);
    }

    @Value("${host}")
    private String host;
    @Value("${application}")
    private String application;

    @RequestMapping("host")
    public String getHost(){
        return "从config-server中读取host app" + host + application;
    }
}
