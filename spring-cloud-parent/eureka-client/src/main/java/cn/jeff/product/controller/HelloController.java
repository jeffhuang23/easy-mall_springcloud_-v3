package cn.jeff.product.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    //访问项目  返回工程启动端口
    @Value("${server.port}")
    private String port;

    @RequestMapping("/client/hello")
    public String sayHi(String name){
        return "hello " + name + " from " + port;
    }
}
