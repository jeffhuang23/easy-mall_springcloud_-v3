package cn.jeff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker //开启断路器  配合service 服务降级的使用
public class StarterRibbon {
    public static void main(String[] args) {
        SpringApplication.run(StarterRibbon.class, args);
    }
        /**
         * 利用ribbon组件, 创建一个容器RestTemplate对象, 添加注解
         * 使用这个对象的所有调用方法都经过ribbon负载均衡计算
         */
        @Bean
        @LoadBalanced //负载均衡的拦截器
        //ribbon组件在这个对象上做一个记号, 一旦这个对象发起http请求
        //经过ribbon的拦截逻辑, 实现微服务的调用
       public RestTemplate initInstance(){
            return new RestTemplate();
    }
}
