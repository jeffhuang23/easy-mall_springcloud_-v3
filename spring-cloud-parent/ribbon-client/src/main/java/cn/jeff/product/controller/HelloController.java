package cn.jeff.product.controller;

import cn.jeff.product.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    private HelloService helloService;
    //ribbon工程可以直接访问的接口
    @RequestMapping("/ribbon/hello")
    public String hello(String name){
        return "ribbon " + helloService.sayHi(name);
    }
}
