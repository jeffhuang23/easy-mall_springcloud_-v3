package cn.jeff.product.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class HelloService {
    @Autowired
    private RestTemplate template;

    //在发起服务调用的方法, 添加服务降级机制
    //进入服务降级的方法, 名称叫error
    @HystrixCommand(fallbackMethod = "error")
    public String sayHi(String name){
           String url = "http://service-hi/client/hello?name="+name;
        String responseBody = template.getForObject(url, String.class);
        return responseBody;
    }

    //服务降级的方法, 必须和服务降级方法的结构一致
    public String error(String name){
        return "sooorrry,  error haapened";
    }
}
