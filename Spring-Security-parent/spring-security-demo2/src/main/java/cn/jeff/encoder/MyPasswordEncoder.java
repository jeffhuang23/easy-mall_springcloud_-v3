package cn.jeff.encoder;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 自定义密码加密
 */
public class MyPasswordEncoder implements PasswordEncoder {
    //密码加密计算方法, 把明文加密成密文
    @Override
    public String encode(CharSequence password) { //接收参数是明文密码
        //CharSequence类型方便进行加密计算
        String s = password.toString();
        return "easymall_"+ s + "_hao";
    }

   //认证时判断用户密码与系统管理的密码是否匹配
    @Override
    public boolean matches(CharSequence charSequence, String s) {
        //String s 是user对象的数据库中的密码
        //charSequence 是登录传递明文密码
        String password = encode(charSequence);
        return s.equals(password);
    }
}
