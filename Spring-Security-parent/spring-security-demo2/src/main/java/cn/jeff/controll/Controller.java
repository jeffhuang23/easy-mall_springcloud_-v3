package cn.jeff.controll;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @RequestMapping("/admin/write")
    public String write(){
        return "写入数据";
    }

    @RequestMapping("/admin/update")
    public String update(){
        return "更新数据";
    }

    @RequestMapping("/admin/delete")
    public String delete(){
        return "删除数据";
    }

    @RequestMapping("/user/read")
    public String read(){
        return "读取数据，不需要权限";
    }

    @RequestMapping("/home")
    public Authentication getAuth(Authentication authentication){
        //如果认证成功, 则authentication对象非空的
        System.out.println(authentication);
        return authentication;
    }
}
