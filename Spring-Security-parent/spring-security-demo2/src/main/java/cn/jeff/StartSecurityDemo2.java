package cn.jeff;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.jeff.mapper")
public class StartSecurityDemo2 {
    public static void main(String[] args) {
        SpringApplication.run(StartSecurityDemo2.class,args);
    }

}
