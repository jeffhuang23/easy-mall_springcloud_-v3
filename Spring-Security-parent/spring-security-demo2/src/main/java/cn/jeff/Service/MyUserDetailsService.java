package cn.jeff.Service;

import cn.jeff.domain.Permission;
import cn.jeff.domain.User;
import cn.jeff.mapper.PermissionMapper;
import cn.jeff.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //通过userName 查询user对象, 查询permission
        // 封装成一个UserDetails对象返回
        User user = userMapper.selectUserByName(username);
        List<Permission> permissions = permissionMapper.selectPermissionsByUserId(user.getId());
        //封装权限对象
        ArrayList<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (Permission p : permissions) {
            //使用对象的authority封装权限list
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(p.getAuthority());
            authorities.add(authority);
        }
        //UserDetails接口和GrantedAuthority接口可以自己实现
        //最终返回一个UserDetails实现类
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);


    }
}
