package cn.jeff.Service;

import cn.jeff.mapper.PermissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

/**
 * 自定义GrantedAuthority实现类
 */
public class MyAuthority implements GrantedAuthority {
    @Autowired
    private PermissionMapper permissionMapper;
    @Override
    public String getAuthority() {
        return null;
    }
}
