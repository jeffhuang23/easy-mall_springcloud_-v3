package cn.jeff.Service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * 自定义UserDetails 实现类
 */
public class MyUserDetails implements UserDetails {
    private String userName;
    private String password;
    private List<MyAuthority> authorityList;

    public MyUserDetails(String userName, String password, List<MyAuthority> authorityList) {
        this.userName = userName;
        this.password = password;
        this.authorityList = authorityList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }


    //自定义的UserDetails类需要重写这两个方法, 因为在进行用户会话管理时, 底层用map存储的key是用户对象, 判断是否为同一个用户对象通过equals和hashcode 方法
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyUserDetails that = (MyUserDetails) o;
        return Objects.equals(userName, that.userName) &&
                Objects.equals(password, that.password) &&
                Objects.equals(authorityList, that.authorityList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, password, authorityList);
    }
}
