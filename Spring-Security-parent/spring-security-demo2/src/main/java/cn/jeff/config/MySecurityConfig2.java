package cn.jeff.config;

import cn.jeff.Service.MyUserDetailsService;
import cn.jeff.encoder.MyPasswordEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 利用JDBC获取数据库用户
 */
@EnableWebSecurity
public class MySecurityConfig2 extends WebSecurityConfigurerAdapter {

    /**
     * 引入自定义用户信息
     */
    @Bean
    public UserDetailsService userDetailsService(){
        return new MyUserDetailsService();
    }

    /**
     * 引入加密器
     */
    @Bean
    public PasswordEncoder myEncoder() {
        //使用已有的密码加密的实现类
//        return new BCryptPasswordEncoder();

        //自定义密码加密
        return new MyPasswordEncoder();
    }
    /**
     * 引入自定义的用户验证条件
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //定义用户认证管理, 怎么去判断用户输入的用户名密码是正确的
        auth.userDetailsService(userDetailsService());
    }



    @Override
    //自定义请求认证的授权逻辑
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        /**
         * antMathcers("")是比对请求地址，可以使用ANT匹配，也可以直接给准确地址。
         */
        //所有请求到当前系统都需要经过认证授权逻辑
        httpSecurity.authorizeRequests()
                //匹配地址  类似ngnix的地址匹配逻辑
                .antMatchers("/user/**")//请求地址以user开始时
                .hasAuthority("read")//以user开始时, 用户权限必须有read权限
                .antMatchers("/admin/write")//匹配admin/write
                .hasAuthority("update")
                .antMatchers("/admin/delete")
                .hasAuthority("delete")
                .anyRequest()
                .authenticated();//其他请求和用户权限关系, 只要登录后就允许访问

        httpSecurity.formLogin();//要求以表单填写用户密码作为登录入口

        //加入Spring-Security会话管理机制
        httpSecurity.sessionManagement().maximumSessions(1);//最大会话并发数,同一个用户最多的sessionId数量
    }
}
