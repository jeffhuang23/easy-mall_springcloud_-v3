package cn.jeff;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {
    @RequestMapping("/home")
    public String hello(String name){
        return "oauth2--Resource from"+ name;
    }
}
