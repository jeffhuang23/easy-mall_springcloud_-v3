package cn.jeff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarterResourceServer {
    public static void main(String[] args) {
        SpringApplication.run(StarterResourceServer.class,args);
    }
}
