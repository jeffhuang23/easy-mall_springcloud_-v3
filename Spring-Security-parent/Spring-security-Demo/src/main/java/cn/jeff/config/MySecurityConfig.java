package cn.jeff.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 自定义认证逻辑,使用自定义内存对象
 */
//自定义Security 配置类
@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {
    //定义一个加密器, 和4.x的区别, 需要手动定义加密器
    //明文加密器, 5.x的security 没有手动定义加密器会报错
    @Bean
    public PasswordEncoder myEncoder() {
        //明文加密的类
        return NoOpPasswordEncoder.getInstance();

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //内存用户权限设置
        //admin 管理员
        auth.inMemoryAuthentication().withUser("admin").
                password("123456").authorities("write", "read", "update", "delete");
        //普通用户
        auth.inMemoryAuthentication().withUser("user")
                .password("user").authorities("read");

    }

    @Override
    //自定义认证的授权逻辑
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        /**
         * antMathcers("")是比对请求地址，可以使用ANT匹配，也可以直接给准确地址。
         */
        //所有请求到当前系统都需要经过认证授权逻辑
        httpSecurity.authorizeRequests()
                //匹配地址  类似ngnix的地址匹配逻辑
                .antMatchers("/user/**")//请求地址以user开始时
                .hasAuthority("read")//以user开始时, 用户权限必须有read权限
                .antMatchers("/admin/write")//匹配admin/write
                .hasAuthority("update")
                .antMatchers("/admin/delete")
                .hasAuthority("delete")
                .anyRequest().authenticated();//其他请求和用户权限关系, 只要登录后就允许访问

        httpSecurity.formLogin();//要求以表单填写用户密码作为登录入口
    }


}
