package cn.jeff;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.jeff.mapper")
public class StarterSessionSecurity {
    public static void main(String[] args) {
        SpringApplication.run(StarterSessionSecurity.class,args);
    }
}
