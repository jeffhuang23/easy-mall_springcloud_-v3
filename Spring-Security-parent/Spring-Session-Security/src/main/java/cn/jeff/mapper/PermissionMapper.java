package cn.jeff.mapper;

import cn.jeff.domain.Permission;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PermissionMapper {
    @Select("select * from tb_permission where user_id=#{userId}")
    public List<Permission> selectPermissionsByUserId(@Param("userId") Long userId);
}
