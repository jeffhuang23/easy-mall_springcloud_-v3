package cn.jeff.mapper;

import cn.jeff.domain.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface UserMapper {
    //使用注解完成sql的编写
    @Select("select * from tb_user where username=#{username}")
    public User selectUserByName(@Param("username") String username);
}
