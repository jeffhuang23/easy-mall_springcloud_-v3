package cn.jeff.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class SessionController {
    //调用session的api,  存储一些域属性
    @RequestMapping("/session")
    public String setAttribute(String name, String value, HttpSession session){
        session.setAttribute(name,value==null?"空":value);
        return "success";
    }

    @RequestMapping("/get")
    public String get(HttpServletRequest request,String name){
        return (String)request.getSession().getAttribute(name);
    }
}
