package cn.jeff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSessionStarter {
    public static void main(String[] args) {
        SpringApplication.run(SpringSessionStarter.class,args);
    }
}
