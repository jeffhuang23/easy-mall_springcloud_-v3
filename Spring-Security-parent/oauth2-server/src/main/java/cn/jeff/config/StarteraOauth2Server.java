package cn.jeff.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarteraOauth2Server {
    public static void main(String[] args) {
        SpringApplication.run(StarteraOauth2Server.class,args);
    }
}
